package com.paic.arch.interviews;

import com.paic.arch.interviews.impl.TimeConverterImpl;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import static com.paic.arch.interviews.support.BehaviouralTestEmbedder.aBehaviouralTestRunner;
import static org.assertj.core.api.Assertions.assertThat;

/**
 * Acceptance test class that uses the JBehave (Gerkin) syntax for writing stories.  
 * You will notice the TimeConverter has no implementation ... (hint)
 */
public class SetTheoryClockDevice {
    private static final Logger logger=LoggerFactory.getLogger(SetTheoryClockDevice.class);
    private TimeConverter setTheoryClock;
    private String theTime;

    @Test
    public void setTheoryClockAcceptanceTests() throws Exception {
        //初始化setTheoryClock
        this.setTheoryClock=new TimeConverterImpl();
        aBehaviouralTestRunner()
                .usingStepsFrom(this)
                .withStory("set-theory-clock.story")
                .run();
    }

    @When("the time is $time")
    public void whenTheTimeIs(String time) {
        theTime = time;
    }

    @Then("the clock should look like $")
    public void thenTheClockShouldLookLike(String theExpectedClockOutput) {
        assertThat(setTheoryClock.convertTime(theTime)).isEqualTo(theExpectedClockOutput);
    }
}
