package com.paic.arch.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;


/**
 * @author 熊浪
 * @Email xiongl@sunxl.cn
 * @Time 2018/2/8
 * @此类的作用：时间辅助类
 */
public class ConverUtil {
    private static final String DAY_STR_TIME = "HH:mm:ss";
    public static final String[] times={"O","R","Y"};

    /**
     * 把字符串HH:mm:ss类型的时间转换为Calendar对象
     * @param time
     * @return
     */
    public static Calendar convertStrToCalendar(String time) {
        SimpleDateFormat sdf = new SimpleDateFormat(DAY_STR_TIME);
        Calendar calendar=Calendar.getInstance();
        try {
            calendar.setTime(sdf.parse(time));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return calendar;
    }

    /**
     * 获取除数和余数，除数为dividend除于divisor，获取第二排和第四排的亮灯个数，余数为dividend除余divisor，表示第三排和第五排亮灯个数
     * @param dividend
     * @param divisor
     * @return
     */
    public static Map<String, Integer> division(int dividend, int divisor) {
        Map<String, Integer> map = new HashMap<>();
        if (divisor == 0)
            throw new NumberFormatException("除数不能为0");
        if (dividend > divisor) {
            map.put("big", dividend / divisor);
        } else {
            map.put("big", 0);
        }
        map.put("small", dividend % divisor);
        return map;
    }

    public static void main(String[] args) {
        System.out.println(0%5);
    }
}
