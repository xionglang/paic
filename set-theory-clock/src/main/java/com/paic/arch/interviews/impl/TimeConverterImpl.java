package com.paic.arch.interviews.impl;

import com.paic.arch.interviews.TimeConverter;
import com.paic.arch.util.ConverUtil;

import java.util.Calendar;
import java.util.Map;


/**
 * @author 熊浪
 * @Email xiongl@sunxl.cn
 * @Time 2018/2/8
 * @此类的作用：实现时间转换
 */
public class TimeConverterImpl implements TimeConverter {
    /**
     * 实现时间转化为字符串
     *
     * @param time
     * @return
     */
    @Override
    public String convertTime(String time) {
        //时,分，秒
        int hour = 0, minute = 0, second = 0;
        //排除24:00:00
        if (!time.startsWith("24")) {
           //把time转换为Calendar时间
            Calendar calendar = ConverUtil.convertStrToCalendar(time);
            //获取time的时分秒
            hour = calendar.get(Calendar.HOUR_OF_DAY);
            minute = calendar.get(Calendar.MINUTE);
            second = calendar.get(Calendar.SECOND);
        } else {
            hour = 24;
        }
        //最上面一个灯，2秒一换，秒钟为奇数关闭为“0”，偶数开启为“Y”
        StringBuffer sb = new StringBuffer();
        if (second % 2 == 0) {
            sb.append(ConverUtil.times[2]);
        } else {
            sb.append(ConverUtil.times[0]);
        }
        //添加换行符
        sb.append("\r\n");
        Map<String, Integer> map = ConverUtil.division(hour, 5);
        //获取上面一个代表5个小时需要亮的灯
        for (int i = 0, len = map.get("big"); i < len; i++) {
            sb.append(ConverUtil.times[1]);
        }
        //补齐不需要亮的灯
        for (int i = 0, len = 4 - map.get("big"); i < len; i++) {
            sb.append(ConverUtil.times[0]);
        }
        sb.append("\r\n");
        //获取上面一个代表一个小时需要亮的灯
        for (int i = 0, len = map.get("small"); i < len; i++) {
            sb.append(ConverUtil.times[1]);
        }
        //补齐不需要亮的灯
        for (int i = 0, len = 4 - map.get("small"); i < len; i++) {
            sb.append(ConverUtil.times[0]);
        }
        sb.append("\r\n");
        map = ConverUtil.division(minute, 5);
        for (int i = 0, len = map.get("big"); i < len; i++) {
            //当i!=0且(i+1)%3不等于0，表示它并非一刻钟，半小时，最后一刻钟
            if (i != 0 && (i + 1) % 3 == 0) {
                sb.append(ConverUtil.times[1]);
            } else {
                sb.append(ConverUtil.times[2]);
            }
        }
        //补齐不需要亮的灯
        for (int i = 0, len = 11 - map.get("big"); i < len; i++) {
            sb.append(ConverUtil.times[0]);
        }
        sb.append("\r\n");
        for (int i = 0, len = map.get("small"); i < len; i++) {
            sb.append(ConverUtil.times[2]);
        }
        //补齐不需要亮的灯
        for (int i = 0, len = 4 - map.get("small"); i < len; i++) {
            sb.append(ConverUtil.times[0]);
        }
        return sb.toString();
    }


}
