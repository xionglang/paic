### Candidate Chinese Name:
* 
 
- - -  
### Please write down some feedback about the question(could be in Chinese):
* 
第一步：判断错误所在，setTheoryClock报空指针
第二步：建立setTheoryClock的实现类，并初始化setTheoryClock
第三步：编写实现方法
实现思路，
1、因为00:00:00和24:00:00在格式化为时间后获取小时都为0所以需要先排除24:00:00
2、格式化时间字符串，获取时分秒
3、第一排2秒一变色，且0为亮者只需要考虑除于2是否为0就可以判断是否灯是否需要变亮。
4、第二排需要和第三排共同看，因为上面一个代表小面5个，所以可以先通过除以5获取上面的值，再通过除余5获取下面的值，不够长度的则表示灯为不亮，补齐。
5、第四和第五排，原理同第四步。
- - -