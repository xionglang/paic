package com.paic.arch.jmsbroker;

import javax.jms.*;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * @author 熊浪
 * @Email xiongl@sunxl.cn
 * @Time 2018/2/9
 * @此类的作用：消息工厂
 */
public class MessageFactory extends TimerTask {
    //消息阻塞队列,定义队列接受10000个请求
    public static final LinkedBlockingQueue<MessageInfo> sendQuence = new LinkedBlockingQueue<MessageInfo>(10000);
    //返回的消息队列信息
    public static final LinkedBlockingQueue<MessageInfo> receiveQuence = new LinkedBlockingQueue<MessageInfo>(10000);
    //启动服务器线程
    private static boolean flag = false;
    //Lock锁启动线程
    private static final Lock lock = new ReentrantLock();

    public static void saveMessage(Session session, Destination destination, String message,Integer timeOut) {
        try {
            sendQuence.put(new MessageInfo(session, destination, message, timeOut));
            if (!flag) {//当默认为false时，启动服务器线程
                try {
                    lock.tryLock();//加锁，已有对象拥有此锁，则直接跳过。
                    TimerTask timerTask = new MessageFactory();
                    //每一秒钟执行一次服务器处理方法
                    new Timer().schedule(timerTask, 0, 1000);
                    //修改flag状态为true
                    flag = true;
                    //释放锁
                    lock.unlock();
                } catch (Exception e) {
                    e.printStackTrace();
                    flag = true;
                    lock.unlock();
                }
            }
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * 统一发送消息,启用一个死循环
     */
    public void serverReadMessage() {
        try {
            MessageProducer producer = null;
            if (!sendQuence.isEmpty()) {
                //发送队列里面的消息
                MessageInfo messageInfo = sendQuence.take();
                producer = messageInfo.getSession().createProducer(messageInfo.getDestination());
                producer.send(messageInfo.getSession().createTextMessage(messageInfo.getMessage()));
                producer.close();
                MessageConsumer consumer = messageInfo.getSession().createConsumer((messageInfo.getDestination()));
                Message message = consumer.receive(messageInfo.getTimeOut());
                receiveQuence.put(new MessageInfo(messageInfo.getSession(),messageInfo.getDestination(),((TextMessage) message).getText(),null));
                consumer.close();
            }
        } catch (InterruptedException | JMSException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run() {
        serverReadMessage();
    }
}

/**
 * 保存客户端的信息
 */
class MessageInfo {
    //客户Session
    private Session session;
    //目标地址名称
    private Destination destination;
    //客户发送的信息
    private String message;
    //消息发送客户端超时时间
    private Integer timeOut;

    public MessageInfo(Session session, Destination destination, String message, Integer timeOut) {
        this.session = session;
        this.destination = destination;
        this.message = message;
        this.timeOut = timeOut;
    }

    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Destination getDestination() {
        return destination;
    }

    public void setDestination(Destination destination) {
        this.destination = destination;
    }

    public Integer getTimeOut() {
        return timeOut;
    }

    public void setTimeOut(Integer timeOut) {
        this.timeOut = timeOut;
    }
}
